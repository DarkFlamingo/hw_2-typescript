import { Action, MovieType } from './common/enums/enums';
import {
    FAVORITE_MOVIE_ID_NAME,
    MOVIE_ID_NAME,
    MOVIE_CARD_ID_NAME,
} from './common/constants/constants';
import {
    createFilmCard,
    createRandomFilmCard,
    createFavoriteFilmCard,
} from './helpers/domHelpers';
import { arrayMapper, itemMapper } from './helpers/mapper';
import { getRandomNumber } from './helpers/mathCalc';
import { likeFilm, dislikeFilm, getFavoriteFilmsId } from './services/storage';
import { Color } from './common/enums/enums';
import { searchFilm, loadFilms, loadFilmById } from './api/movieApi';

let randomFilmId: number | null = null;

function removeFavoritCard(id: number): void {
    const filmFavoriteCard = document.getElementById(MOVIE_CARD_ID_NAME + id);
    if (filmFavoriteCard) {
        filmFavoriteCard.remove();
    }
}

function transformLike(id: number, isLiked: boolean): void {
    const likeBtn = document.getElementById(MOVIE_ID_NAME + id);
    if (likeBtn) {
        const color = isLiked ? Color.RED : Color.LITE_RED;
        likeBtn.setAttribute('fill', color);
        likeBtn.addEventListener(Action.CLICK, () => {
            handleLikeOrDislike(id, isLiked);
        });
    }
    removeFavoritCard(id);
}

function handleLikeOrDislike(id: number, isLiked: boolean): void {
    if (isLiked) {
        dislikeFilm(id);
        transformLike(id, false);
    } else {
        likeFilm(id);
        transformLike(id, true);
    }
}

function deleteDisabled(id: string): void {
    document.getElementById(id)?.removeAttribute('disabled');
}

function addDisabled(id: string): void {
    document.getElementById(id)?.setAttribute('disabled', 'true');
}

function showFavoriteFilms(): void {
    const favoriteFilms = getFavoriteFilmsId();
    const favoriteFilmsContainer = document.getElementById('favorite-movies');
    if (favoriteFilmsContainer) favoriteFilmsContainer.innerHTML = '';
    favoriteFilms.forEach((el) => showFavoriteFilm(el));
}

function showSearchedFilm(page: number, searchText: string): void {
    searchFilm(searchText, page).then((data) => {
        const page: number = data.page;
        const filmArray = arrayMapper(data.results);
        const filmContainer = document.getElementById('film-container');
        if (filmContainer && page === 1) {
            filmContainer.innerHTML = '';
        }
        const items = localStorage.getItem('likedFilms');
        let likedFilms: number[];
        if (items) {
            likedFilms = JSON.parse(items);
        }
        if (filmContainer) {
            filmArray.forEach((el) => {
                let isLiked = false;
                if (likedFilms && likedFilms.indexOf(el.id) != -1) {
                    isLiked = true;
                }
                filmContainer.appendChild(createFilmCard(el, isLiked));
                document
                    .getElementById(MOVIE_ID_NAME + el.id)
                    ?.addEventListener(Action.CLICK, () => {
                        handleLikeOrDislike(el.id, isLiked);
                    });
            });
        }
        const btnMore = document.getElementById('load-more');
        if (btnMore) {
            const newBtn = btnMore.cloneNode(true);
            btnMore.parentNode?.replaceChild(newBtn, btnMore);
            document
                .getElementById('load-more')
                ?.addEventListener(Action.CLICK, () =>
                    showSearchedFilm(page + 1, searchText)
                );
        }
    });
}

function showLoadedFilms(page: number, filmType: string): void {
    loadFilms(filmType, page).then((data) => {
        const page: number = data.page;
        const filmArray = arrayMapper(data.results);
        const filmContainer = document.getElementById('film-container');
        if (filmContainer && page === 1) {
            filmContainer.innerHTML = '';
            if (!randomFilmId) {
                const randomId = getRandomNumber(0, 19);
                randomFilmId = filmArray[randomId].id;
                showRandomFilm(randomFilmId);
            }
        }
        const items = localStorage.getItem('likedFilms');
        let likedFilms: number[];
        if (items) {
            likedFilms = JSON.parse(items);
        }
        if (filmContainer) {
            filmArray.forEach((el) => {
                let isLiked = false;
                if (likedFilms && likedFilms.indexOf(el.id) != -1) {
                    isLiked = true;
                }
                filmContainer.appendChild(createFilmCard(el, isLiked));
                document
                    .getElementById(MOVIE_ID_NAME + el.id)
                    ?.addEventListener(Action.CLICK, () => {
                        handleLikeOrDislike(el.id, isLiked);
                    });
            });
        }
        const btnMore = document.getElementById('load-more');
        if (btnMore) {
            const newBtn = btnMore.cloneNode(true);
            btnMore.parentNode?.replaceChild(newBtn, btnMore);
            document
                .getElementById('load-more')
                ?.addEventListener(Action.CLICK, () =>
                    showLoadedFilms(page + 1, filmType)
                );
        }
    });
}

function showRandomFilm(id: number): void {
    loadFilmById(id).then((data) => {
        const film = itemMapper(data);
        const randomMovie = document.getElementById('random-movie');
        if (randomMovie) {
            if (film.backgroundPath) {
                randomMovie.setAttribute(
                    'style',
                    `background-image: url(${film.backgroundPath})`
                );
            }
            randomMovie.appendChild(createRandomFilmCard(film));
        }
    });
}

function showFavoriteFilm(id: number): void {
    loadFilmById(id).then((data) => {
        const film = itemMapper(data);
        const favoriteFilms = document.getElementById('favorite-movies');
        if (favoriteFilms) {
            favoriteFilms.appendChild(createFavoriteFilmCard(film, true));
            document
                .getElementById(FAVORITE_MOVIE_ID_NAME + id)
                ?.addEventListener(Action.CLICK, () => {
                    handleLikeOrDislike(id, true);
                });
        }
    });
}

export async function render(): Promise<void> {
    document.addEventListener(Action.DOM_CONTENT_LOADED, () => {
        addDisabled(MovieType.POPULAR);
        deleteDisabled(MovieType.TOP_RATED);
        deleteDisabled(MovieType.UPCOMING);

        showLoadedFilms(1, MovieType.POPULAR);

        document
            .getElementById(MovieType.TOP_RATED)
            ?.addEventListener(Action.CLICK, () => {
                addDisabled(MovieType.TOP_RATED);
                deleteDisabled(MovieType.UPCOMING);
                deleteDisabled(MovieType.POPULAR);

                showLoadedFilms(1, MovieType.TOP_RATED);
            });

        document
            .getElementById(MovieType.UPCOMING)
            ?.addEventListener(Action.CLICK, () => {
                addDisabled(MovieType.UPCOMING);
                deleteDisabled(MovieType.POPULAR);
                deleteDisabled(MovieType.TOP_RATED);

                showLoadedFilms(1, MovieType.UPCOMING);
            });

        document
            .getElementById(MovieType.POPULAR)
            ?.addEventListener(Action.CLICK, () => {
                addDisabled(MovieType.POPULAR);
                deleteDisabled(MovieType.TOP_RATED);
                deleteDisabled(MovieType.UPCOMING);

                showLoadedFilms(1, MovieType.POPULAR);
            });

        document
            .getElementById('submit')
            ?.addEventListener(Action.CLICK, () => {
                addDisabled('submit');
                const input = <HTMLInputElement>(
                    document.getElementById('search')
                );
                showSearchedFilm(1, input.value);
                deleteDisabled('submit');
            });

        document
            .getElementById('favorite-films-btn')
            ?.addEventListener(Action.CLICK, () => {
                showFavoriteFilms();
            });
    });
}
