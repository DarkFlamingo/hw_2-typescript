import { BASE_IMAGE_URL } from '../common/constants/constants';

export function arrayMapper(res: any): IMovie[] {
    const array: IMovie[] = [];
    res.forEach((el: any) => {
        array.push(itemMapper(el));
    });
    return array;
}

export function itemMapper(item: any): IMovie {
    return <IMovie>{
        id: item.id,
        overview: item.overview,
        posterPath: BASE_IMAGE_URL + item.poster_path,
        backgroundPath: BASE_IMAGE_URL + item.backdrop_path,
        releaseDate: item.release_date,
        title: item.title,
    };
}
