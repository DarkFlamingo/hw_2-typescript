import {
    FAVORITE_MOVIE_ID_NAME,
    MOVIE_ID_NAME,
    MOVIE_CARD_ID_NAME,
} from '../common/constants/constants';

import {
    Color
} from '../common/enums/enums';

function createElement(
    tagName: string,
    className: string,
    attributes: IDictionary = {}
): HTMLElement {
    const element = document.createElement(tagName);

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(attributes).forEach((key) =>
        element.setAttribute(key, attributes[key])
    );

    return element;
}

function createFilmBaseCard(
    film: IMovie,
    isLiked: boolean,
    addition: string
): HTMLElement {
    const img = createElement('img', '', {
        src: film.posterPath,
        alt: 'There is no poster image... (',
    });

    const body = createElement('div', 'card-body');

    const bodyText = createElement('p', 'card-text truncate');
    bodyText.textContent = film.overview;

    const additionalTextContainer = createElement(
        'div',
        'd-flex justify-content-between align-items-center'
    );
    const additionalText = createElement('small', 'text-muted');
    additionalText.textContent = film.releaseDate;
    additionalTextContainer.appendChild(additionalText);

    body.appendChild(bodyText);
    body.appendChild(additionalTextContainer);

    const wrapper = createElement('div', 'card shadow-sm');

    wrapper.appendChild(img);
    const color = isLiked ? Color.RED : Color.LITE_RED;
    wrapper.innerHTML += `<svg id="${
        addition + film.id
    }" xmlns="http://www.w3.org/2000/svg" stroke=${Color.RED} fill="${color}" width="50" height="50" class="bi bi-heart-fill position-absolute p-2" viewBox="0 -2 18 22"><path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/></svg>`;
    wrapper.appendChild(body);

    return wrapper;
}

export function createFilmCard(film: IMovie, isLiked: boolean): HTMLElement {
    const wrapper = createFilmBaseCard(film, isLiked, MOVIE_ID_NAME);
    const el = createElement('div', 'col-lg-3 col-md-4 col-12 p-2');
    el.appendChild(wrapper);
    return el;
}

export function createFavoriteFilmCard(
    film: IMovie,
    isLiked: boolean
): HTMLElement {
    const wrapper = createFilmBaseCard(
        film,
        isLiked,
        FAVORITE_MOVIE_ID_NAME
    );
    const el = createElement('div', 'col-12 p-2', {
        id: `${MOVIE_CARD_ID_NAME + film.id}`,
    });
    el.appendChild(wrapper);
    return el;
}

export function createRandomFilmCard(film: IMovie): HTMLElement {
    const name = createElement('h1', 'fw-light text-light', {
        id: 'random-movie-name',
    });
    name.textContent = film.title;

    const description = createElement('p', 'lead text-white', {
        id: 'random-movie-description',
    });
    description.textContent = film.overview;

    const backgroundImage = createElement('div', 'col-lg-6 col-md-8 mx-auto');
    backgroundImage.appendChild(name);
    backgroundImage.appendChild(description);

    const el = createElement('div', 'row py-lg-5');
    el.appendChild(backgroundImage);
    return el;
}
