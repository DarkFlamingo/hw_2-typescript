import { LOCAL_STORAGE_FILMS_ARRAY_NAME } from '../common/constants/constants';

export function likeFilm(id: number): void {
  const items = localStorage.getItem(LOCAL_STORAGE_FILMS_ARRAY_NAME);
  let likedFilms: number[];
  if (items) {
      likedFilms = JSON.parse(items);
  } else {
      likedFilms = [];
  }
  const index = likedFilms.indexOf(id);
  if (index == -1) {
      likedFilms.push(id);
  }
  localStorage.setItem(LOCAL_STORAGE_FILMS_ARRAY_NAME, JSON.stringify(likedFilms));
}

export function dislikeFilm(id: number): void {
  const items = localStorage.getItem(LOCAL_STORAGE_FILMS_ARRAY_NAME);
  let likedFilms: number[];
  if (items) {
      likedFilms = JSON.parse(items);
      const index = likedFilms.indexOf(id);
      if (index != -1) {
          likedFilms.splice(index, 1);
      }
      localStorage.setItem(LOCAL_STORAGE_FILMS_ARRAY_NAME, JSON.stringify(likedFilms));
  } else {
      return;
  }
}

export function getFavoriteFilmsId(): number[] {
  const items = localStorage.getItem(LOCAL_STORAGE_FILMS_ARRAY_NAME);
  if (items) {
      return <number[]>JSON.parse(items);
  } else {
      return [];
  }
}