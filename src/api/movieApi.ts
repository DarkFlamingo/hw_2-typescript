import { BASE_URL, API_KEY } from '../common/constants/constants';
import axios from 'axios';

export async function searchFilm(
    searchText: string,
    page: number
): Promise<any> {
    return axios
        .get(
            `${BASE_URL}/search/movie?api_key=${API_KEY}&language=en-US&query=${searchText}}&page=${page}&include_adult=false`
        )
        .then(({ data }) => data);
}

export async function loadFilms(filmType: string, page: number): Promise<any> {
    return await axios
        .get(
            `${BASE_URL}/movie/${filmType}?api_key=${API_KEY}&language=en-US&page=${page}`
        )
        .then(({ data }) => data);
}

export async function loadFilmById(id: number): Promise<any> {
    return await axios
        .get(`${BASE_URL}/movie/${id}?api_key=${API_KEY}&language=en-US`)
        .then(({ data }) => data);
}