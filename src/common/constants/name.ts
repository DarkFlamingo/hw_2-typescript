const FAVORITE_MOVIE_ID_NAME = 'like-favorite-';
const MOVIE_ID_NAME = 'like-common-';
const MOVIE_CARD_ID_NAME = 'favorite-card-';
const LOCAL_STORAGE_FILMS_ARRAY_NAME = 'likedFilms';

export { FAVORITE_MOVIE_ID_NAME, MOVIE_ID_NAME, MOVIE_CARD_ID_NAME, LOCAL_STORAGE_FILMS_ARRAY_NAME };
