const BASE_IMAGE_URL = 'https://image.tmdb.org/t/p/original';
const BASE_URL = 'https://api.themoviedb.org/3';

export { BASE_URL, BASE_IMAGE_URL };
