interface IMovie {
    id: number;
    overview: string;
    posterPath: string;
    backgroundPath: string;
    releaseDate: string;
    title: string;
}
