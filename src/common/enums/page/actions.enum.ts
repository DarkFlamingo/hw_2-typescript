enum Action {
    DOM_CONTENT_LOADED = 'DOMContentLoaded',
    CLICK = 'click',
}

export { Action }